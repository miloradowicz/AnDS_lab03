
#define NOMINMAX

#include <iostream>
#include <unordered_map>
#include <string>
#include <Windows.h>
#include "AnDS.h"
#include "server.h"

struct myClass
{
  int _myInt;

  bool operator <(const myClass &op) const
  {
    return _myInt < op._myInt;
  }

  operator int() const
  {
    return _myInt;
  }

  friend class myCustomComparator;
};

struct myCustomComparator
{
  bool operator ()(const myClass &op1, const myClass &op2)
  {
    return op1 < op2;
  }
};

const std::unordered_map<std::string, int> commands {
  { "push", 0 },
  { "top", 1 },
  { "pop", 2 },
  { "quit", 3 }
};

int main(int argc, char *argv[])
{
  typedef AnDS::priority_queue<int> my_queue;
  typedef AnDS::priority_queue<int, std::greater<int>> my_max_queue;
  typedef AnDS::priority_queue<myClass, myCustomComparator> my_custom_queue;
  my_queue q;
  server srv;
  HWND hWnd;

  SetConsoleTitle(L"AnDS Lab-03 Console");
  srv.initialize();
  hWnd = GetConsoleWindow();
  SetWindowPos(hWnd, 0, 200, 300, 0, 0, SWP_NOSIZE);
  {
    std::cout << "\n"
      << "The visualizer only supports integer-based queues and so does the server class\n\n"
      << "Use the following commands to operate the queue:\n"
      << "   push 123 -- to push 123 into the queue\n"
      << "   top      -- to print the topmost element\n"
      << "   pop      -- to remove the topmost element without printing it\n"
      << "   quit     -- to exit the program\n\n"
      << "The interface of my priority queue was modeled after its C++ STL counterpart,\nthat's why I chose the pop method not to return anything." << std::endl;
  }
  bool repeat = true;
  while (repeat)
  {
    std::string c, a;
    std::cin >> c;
    if (commands.find(c) == commands.end())
    {
      std::cout << "Unrecognized command" << std::endl;
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      continue;
    }
    switch (commands.at(c))
    {
    case 0:
    {
      try
      {
        std::cin >> a;
        q.push(std::stoul(a));
        srv.send_data(q._raw());
      }
      catch (const std::invalid_argument &e)
      {
        std::cout << "Could not convert the argument into a number" << std::endl;
        continue;
      }
      break;
    }
    case 1:
    {
      try
      {
        std::cout << q.top() << std::endl;
      }
      catch (const my_queue::priority_queue_empty &e)
      {
        std::cout << "The queue is empty" << std::endl;
      }
      break;
    }
    case 2:
    {
      try
      {
        q.pop();
        srv.send_data(q._raw());
      }
      catch (const my_queue::priority_queue_empty &e)
      {
        std::cout << "The queue is empty" << std::endl;
      }
      break;
    }
    case 3:
    {
      repeat = false;
      srv.send_close();
      break;
    }
    }
  }

  return 0;
}