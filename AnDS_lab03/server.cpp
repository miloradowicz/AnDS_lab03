
#include "server.h"

std::wstring server::get_random_string(int len)
{
  static const wchar_t alphanum[] = L"0123456789abcdefghijklmnopqrstuvwxyz";
  static const int alphanum_l = (sizeof(alphanum) - sizeof(wchar_t)) / sizeof(wchar_t);
  static int round = 0;

  srand(GetTickCount() * round++ * GetCurrentProcessId());
  std::wstring s(len, L' ');
  for (int i = 0; i < len; i++)
    s[i] = alphanum[rand() % alphanum_l];

  return s;
}

server::server()
{
  ZeroMemory(&si, sizeof(si));
  si.cb = sizeof(si);
  si.lpTitle = (LPWSTR)L"AnDS Lab-03 Visualizer";
  si.dwX = 10;
  si.dwY = 10;
  si.dwFlags = STARTF_USEPOSITION | STARTF_USESHOWWINDOW;
  si.wShowWindow = SW_SHOWNOACTIVATE;
  ZeroMemory(&pi, sizeof(pi));
  cmdPipe = dataPipe = INVALID_HANDLE_VALUE;
}

server::~server()
{
  if (cmdPipe != INVALID_HANDLE_VALUE)
  {
    static const int cmd_close = -1;
    if (!WriteFile(
      cmdPipe,
      (char *)&cmd_close,
      sizeof(int),
      NULL,
      NULL
    ))
    {
      std::cout << "Error on the command pipe" << std::endl;
    }
    DisconnectNamedPipe(cmdPipe);
    CloseHandle(cmdPipe);
  }
  if (dataPipe != INVALID_HANDLE_VALUE)
  {
    DisconnectNamedPipe(dataPipe);
    CloseHandle(dataPipe);
  }
}

void server::initialize()
{
  std::wstring cmdPipeName = projectName + L'-' + get_random_string(16);
  std::wstring dataPipeName = projectName + L'-' + get_random_string(16);
  std::wstring cmdLine = L' ' + cmdPipeName + L' ' + dataPipeName;
  wchar_t buf[MAX_PATH] = { };
  GetModuleFileName(NULL, buf, MAX_PATH);
  std::wstring programPath = std::wstring(buf);
  std::wstring programDirectory = programPath.substr(0, programPath.find_last_of(L'\\') + 1);
  std::wstring clientPath = programDirectory + clientProgramName;
  cmdPipe = CreateNamedPipe(
    (localPipePrefix + cmdPipeName).c_str(),
    PIPE_ACCESS_DUPLEX,
    PIPE_TYPE_BYTE | PIPE_READMODE_BYTE,
    2,
    4096,
    4096,
    0,
    NULL
  );
  if (cmdPipe == INVALID_HANDLE_VALUE)
  {
    std::cout << "Couldn't create the command pipe" << std::endl;
    return;
  }
  dataPipe = CreateNamedPipe(
    (localPipePrefix + dataPipeName).c_str(),
    PIPE_ACCESS_OUTBOUND,
    PIPE_TYPE_BYTE | PIPE_READMODE_BYTE,
    2,
    4096,
    0,
    0,
    NULL
  );
  if (dataPipe == INVALID_HANDLE_VALUE)
  {
    std::cout << "Couldn't create the data pipe" << std::endl;
    CloseHandle(cmdPipe);
    cmdPipe = INVALID_HANDLE_VALUE;
    return;
  }
  if (!CreateProcess(
    clientPath.c_str(),
    (LPWSTR)cmdLine.c_str(),
    NULL,
    NULL,
    TRUE,
    CREATE_NEW_CONSOLE,
    NULL,
    NULL,
    &si,
    &pi
  ))
  {
    std::cout << "Could not start the visualizer" << std::endl;
    CloseHandle(cmdPipe);
    CloseHandle(dataPipe);
    cmdPipe = dataPipe = INVALID_HANDLE_VALUE;
    return;
  }

  std::cout << "Awaiting connection on the command pipe...";
  if (ConnectNamedPipe(cmdPipe, NULL))
  {
    std::cout << "connected" << std::endl;
  }
  else
  {
    std::cout << "Error: " << GetLastError();
    CloseHandle(cmdPipe);
    CloseHandle(dataPipe);
    cmdPipe = dataPipe = INVALID_HANDLE_VALUE;
    return;
  }
  std::cout << "Awaiting connection on the data pipe......";
  if (ConnectNamedPipe(dataPipe, NULL))
  {
    std::cout << "connected" << std::endl;
  }
  else
  {
    std::cout << "Error: " << GetLastError();
    DisconnectNamedPipe(cmdPipe);
    CloseHandle(cmdPipe);
    CloseHandle(dataPipe);
    cmdPipe = dataPipe = INVALID_HANDLE_VALUE;
    return;
  }
}

void server::send_data(std::vector<int> raw)
{
  int length = raw.size();
  int *data = raw.data();
  if (!WriteFile(
    cmdPipe,
    (char *)&length,
    sizeof(int),
    NULL,
    NULL
  ))
  {
    std::cout << "Error on the command pipe" << std::endl;
  }
  if (!WriteFile(
    dataPipe,
    (char *)data,
    length * sizeof(int),
    NULL,
    NULL
  ))
  {
    std::cout << "Error on the data pipe" << std::endl;
  }
}

void server::send_close()
{

}