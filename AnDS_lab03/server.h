
#ifndef ANDS_SERVER_H
#define ANDS_SERVER_H

#pragma once
#include <chrono>
#include <iostream>
#include <vector>
#include <string>
#include <Windows.h>

class server
{
  const std::wstring projectName = L"AnDS_lab03";
  const std::wstring clientProgramName = L"visualizer.exe";
  const std::wstring localPipePrefix = L"\\\\.\\pipe\\";

  STARTUPINFO si;
  PROCESS_INFORMATION pi;
  HANDLE cmdPipe, dataPipe;

  static std::wstring get_random_string(int len);

public:
  server();
  ~server();
  void initialize();
  void send_data(std::vector<int> raw);
  void send_close();
};

#endif