
#pragma once
#include "pch.h"

#ifndef ANDS_HEAP
#define ANDS_HEAP

namespace AnDS
{
  template <class T, class Compare = std::less<T>>
  class heap
  {
  public:
    class heap_empty : std::exception { };
    class log_of_zero : std::exception { };

  private:
    std::vector<T> _heap;
    Compare *_comp;

    int             _size() const;
    void            _reserve(int);
    void            _resize(int);
    void            _upheap(int);
    void            _downheap(int);
    T               _peek_first() const;
    void            _append(const T &);
    void            _remove_last();
    void            _swap_two(int, int);
    void            _swap_last(int);

  public:
    heap(int n = 8);
    heap(int n, T *);
    heap(std::vector<T>);
    T               top() const;
    bool            empty() const;
    int             size() const;
    void            push(const T &val);
    void            pop();
    std::vector<T>  _raw();
  };
}

#include "heap.inl"

#endif