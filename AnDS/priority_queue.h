
#pragma once
#include "pch.h"

#ifndef ANDS_PRIORITY_QUEUE
#define ANDS_PRIORITY_QUEUE

#include "heap.h"

namespace AnDS
{
  template <class T, class Compare = std::less<T>>
  class priority_queue
  {
  public:
    class priority_queue_empty : std::exception { };

  private:
    typedef heap<T, Compare> _heap_t;
    _heap_t _heap;

  public:
    priority_queue();
    T               top() const;
    bool            empty() const;
    int             size() const;
    void            push(const T &val);
    void            pop();
    std::vector<T>  _raw();
  };
}

#include "priority_queue.inl"

#endif