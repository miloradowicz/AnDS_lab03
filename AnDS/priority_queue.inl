
#include "priority_queue.h"

namespace AnDS
{
  template <class T, class Compare>
  priority_queue<T, Compare>::priority_queue()
  {

  }

  template <class T, class Compare>
  T priority_queue<T, Compare>::top() const
  {
    try
    {
      return _heap.top();
    }
    catch (const _heap_t::heap_empty &e)
    {
      throw priority_queue_empty();
    }
  }

  template <class T, class Compare>
  bool priority_queue<T, Compare>::empty() const
  {
    return _heap.empty();
  }

  template <class T, class Compare>
  int priority_queue<T, Compare>::size() const
  {
    return _heap._size();
  }

  template <class T, class Compare>
  void priority_queue<T, Compare>::push(const T &val)
  {
    _heap.push(val);
  }

  template <class T, class Compare>
  void priority_queue<T, Compare>::pop()
  {
    try
    {
      _heap.pop();
    }
    catch (const _heap_t::heap_empty &e)
    {
      throw priority_queue_empty();
    }
  }

  template <class T, class Compare>
  std::vector<T> priority_queue<T, Compare>::_raw()
  {
    return _heap._raw();
  }
}