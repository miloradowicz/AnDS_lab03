
#include "heap.h"

namespace AnDS
{
  template <class T, class Compare>
  heap<T, Compare>::heap(int n)
  {
    _reserve(n);
    _comp = new Compare();
  }

  template <class T, class Compare>
  heap<T, Compare>::heap(int n, T *a)
  {
    _resize(n);
    _comp = new Compare();
    for (int i = 0; i < n; i++)
    {
      _append(a[i]);
      for (int j = i - 1 >> 1; j >= 0; --j >>= 1)
        _downheap(j);
    }
  }

  template <class T, class Compare>
  heap<T, Compare>::heap(std::vector<T> a)
  {
    _resize(a.size());
    _comp = new Compare();
    for (int i = 0; i < size(); i++)
    {
      _append(a[i]);
      for (int j = i - 1 >> 1; j >= 0; --j >>= 1)
        _downheap(j);
    }
  }

  template <class T, class Compare>
  int heap<T, Compare>::_size() const
  {
    return _heap.size();
  }

  template <class T, class Compare>
  void heap<T, Compare>::_reserve(int n)
  {
    _heap.reserve(n);
  }

  template <class T, class Compare>
  void heap<T, Compare>::_resize(int n)
  {
    _heap.resize(n);
  }

  template <class T, class Compare>
  void heap<T, Compare>::_upheap(int i)
  {
    if (i == 0)
      return;

    int top, p;
    p = i - 1 >> 1;

    if ((*_comp)(_heap[i], _heap[p]))
      top = i;
    else
      top = p;

    if (top != p)
    {
      _swap_two(top, p);
      _upheap(p);
    }
  }
  template <class T, class Compare>
  void heap<T, Compare>::_downheap(int i)
  {
    int left, right, top;
    top = i;
    left = (i << 1) + 1;
    right = (i << 1) + 2;

    if (right < _size())
    {
      if ((*_comp)(_heap[left], _heap[top]))
        top = left;
      if ((*_comp)(_heap[right], _heap[top]))
        top = right;
    }
    else if (left < _size())
    {
      if ((*_comp)(_heap[left], _heap[top]))
        top = left;
    }
    else
    {
      return;
    }

    if (top != i)
    {
      _swap_two(top, i);
      _downheap(top);
    }
  }

  template <class T, class Compare>
  T heap<T, Compare>::_peek_first() const
  {
    return _heap.front();
  }

  template <class T, class Compare>
  void heap<T, Compare>::_append(const T &v)
  {
    _heap.push_back(v);
  }

  template <class T, class Compare>
  void heap<T, Compare>::_remove_last()
  {
    _heap.pop_back();
  }

  template <class T, class Compare>
  void heap<T, Compare>::_swap_two(int i, int j)
  {
    std::swap(_heap[i], _heap[j]);
  }

  template <class T, class Compare>
  void heap<T, Compare>::_swap_last(int i)
  {
    std::swap(_heap.front(), _heap.back());
  }

  template <class T, class Compare>
  T heap<T, Compare>::top() const
  {
    if (_size() != 0)
      return _peek_first();
    else
      throw heap_empty();
  }

  template <class T, class Compare>
  bool heap<T, Compare>::empty() const
  {
    return _size() == 0;
  }

  template <class T, class Compare>
  int heap<T, Compare>::size() const
  {
    return _size();
  }

  template <class T, class Compare>
  void heap<T, Compare>::push(const T &v)
  {
    _append(v);
    _upheap(_size() - 1);
  }

  template <class T, class Compare>
  void heap<T, Compare>::pop()
  {
    if (_size() != 0)
    {
      _swap_last(0);
      _remove_last();
      _downheap(0);
    }
    else
    {
      throw heap_empty();
    }
  }

  template <class T, class Compare>
  std::vector<T> heap<T, Compare>::_raw()
  {
    return _heap;
  }
}