﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Runtime.InteropServices;
using System.Threading;

namespace client
{
  class Program
  {
    class LogArgZero : Exception { };
    class LogArgNegative : Exception { };
    class NegativeArg : Exception { };
    class ArgTooBig : Exception { };

    const string normal = "\u001b[0m";
    const string changed = "\u001b[44m";
    const string error = "\u001b[41m";

    const int STD_OUTPUT_HANDLE = -11;
    const int ENABLE_VIRTUAL_TERMINAL_PROCESSING = 0x0004;

    [DllImport("kernel32.dll")]
    extern static IntPtr GetStdHandle(int nStdHandle);
    [DllImport("kernel32.dll")]
    extern static bool GetConsoleMode(IntPtr hConsoleHandle, out int lpMode);
    [DllImport("kernel32.dll")]
    extern static bool SetConsoleMode(IntPtr hConsoleHandle, int dwMode);

    static int[] lp = new int[] { };

    static int Log2(int m)
    {
      if (m < 0)
        throw new LogArgNegative();
      if (m == 0)
        throw new LogArgZero();
      int i = 0;
      while ((m >>= 1) != 0)
        i++;
      return i;
    }
    static int Width(int m)
    {
      if (m < 0)
        throw new NegativeArg();
      else if (m < 10)
        return 0;
      else if (m < 100)
        return 1;
      else if (m < 1000)
        return 2;
      else
        throw new ArgTooBig();
    }
    static void Print(params int[] p)
    {
      int len = Math.Min(p.Length, lp.Length);
      Console.Clear();
      if (p.Length == 0)
      {
        Console.WriteLine("The queue is empty");
      }
      else if (p.Length > 63)
      {
        Console.WriteLine("The tree is too big");
      }
      else
      {
        int h = Log2(p.Length);
        for (int i = 0, j = 0, k = 0, l = 0; i < p.Length; i++)
        {
          if (i == (1 << Log2(i + 1)) - 1)
          {
            j = Log2(i + 1);
            k = (1 << (h - j) +2) + (1 << (h - j) + 1) - 3;
            l = 0;
          }
          if (i != 0 && (i & 1) == 1)
          {
            if (i != p.Length - 1)
            {
              Console.SetCursorPosition(l + (k >> 1), (j << 1) - 1);
              Console.Write("┌");
              Console.Write("".PadLeft(k + 1 >> 1, '─'));
              Console.Write("┴");
              Console.Write("".PadLeft(k + 1 >> 1, '─'));
              Console.Write("┐");
            }
            else
            {
              Console.SetCursorPosition(l + (k >> 1), (j << 1) - 1);
              Console.Write("┌");
              Console.Write("".PadLeft(k + 1 >> 1, '─'));
              Console.Write("┘");
            }
          }
          try
          {
            Console.SetCursorPosition(l + (k >> 1) - (Width(p[i]) < 1 ? 0 : 1), j << 1);
            string str;
            if (i >= len || lp[i] != p[i])
              str = $"{changed}{p[i]}{normal}";
            else
              str = $"{p[i]}";
            Console.Write(str);
          }
          catch (ArgTooBig e)
          {
            Console.SetCursorPosition(l + (k >> 1) - 1, j << 1);
            Console.Write($"{error}???{normal}");
          }
          l += k + 3;
        }
      }
      lp = p;
    }
    static void Main(string[] args)
    {
      if (args.Length < 2)
        return;
      string cmdPipeName = args[0];
      string dataPipeName = args[1];

      Console.SetBufferSize(200, 400);
      Console.SetWindowSize(200, 40);
      IntPtr stdOut = GetStdHandle(STD_OUTPUT_HANDLE);
      int stdOutMode;
      GetConsoleMode(stdOut, out stdOutMode);
      SetConsoleMode(stdOut, stdOutMode | ENABLE_VIRTUAL_TERMINAL_PROCESSING);

      using (NamedPipeClientStream cmdPipe = new NamedPipeClientStream(".", cmdPipeName, PipeDirection.InOut, PipeOptions.None),
        dataPipe = new NamedPipeClientStream(".", dataPipeName, PipeDirection.In, PipeOptions.None))
      {
        //Console.WriteLine("This is your chance to attach a debugger. Press any key to continue...");
        //Console.ReadKey(true);
        Thread.Sleep(250);
        Console.Write("Connecting to the command pipe...");
        cmdPipe.Connect();
        Console.WriteLine("connected");
        Thread.Sleep(250);
        Console.Write("Connecting to the data pipe......");
        dataPipe.Connect();
        Console.WriteLine("connected");
        Console.WriteLine($"\nThis window will display the heap in a tree-like form as below:\n\n" +
          $"         {changed}100{normal}\n    ┌─────┴─────┐\n   {changed}140{normal}         253\n ┌──┴──┐\n{changed}253{normal}   141\n\n" +
          $"where {changed}123{normal} are the elements that were added or repositioned within the heap as the result of the previous operation.\n\n" +
          $"An element will be displayed as {error}???{normal} if its decimal representation is longer than 3 digits.");

        int length;
        using (BinaryReader cmdBr = new BinaryReader(cmdPipe),
        dataBr = new BinaryReader(dataPipe))
        {
          try
          {
            while ((length = cmdBr.ReadInt32()) != -1)
            {
              int[] p = new int[length];
              for (int i = 0; i < length; i++)
                p[i] = dataBr.ReadInt32();
              Print(p);
            }
          }
          catch (EndOfStreamException e)
          {
            Console.WriteLine("Error reading data from one of the pipes. Perhaps the pipe was closed from the server side.");
          }
        }
        cmdPipe.Close();
        dataPipe.Close();
      }
      return;
    }
  }
}